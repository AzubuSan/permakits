package me.azubusan.KitCommands;

import java.util.ArrayList;
import java.util.List;

import me.azubusan.PermaKits.PermaKits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class Archer implements CommandExecutor, Listener {

	public static PermaKits plugin;

	public Archer(PermaKits instance) {
		plugin = instance;
	}

	boolean active = false;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players can get kits!");
			return true;
		}

		final Player p = (Player) sender;
		PlayerInventory pi = p.getInventory();

		if (cmd.getName().equalsIgnoreCase("archer")) {
			if (active == false) { // We all love itemstacks! :D
				ItemStack feet = new ItemStack(Material.LEATHER_BOOTS);
				ItemStack legs = new ItemStack(Material.LEATHER_LEGGINGS);
				ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE);
				ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
				ItemStack weapon = new ItemStack(Material.BOW, 1);
				weapon.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
				weapon.addEnchantment(Enchantment.ARROW_INFINITE, 1);
				ItemStack ammo = new ItemStack(Material.ARROW, 1);
				ItemMeta weaponmeta = weapon.getItemMeta();
				weaponmeta.setDisplayName(ChatColor.DARK_PURPLE
						+ "Archer's Trusty Bow");
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.GREEN + "Archer kit bow");
				ItemMeta ammometa = ammo.getItemMeta();
				ammometa.setDisplayName(ChatColor.AQUA
						+ "Archer's Infinity Arrow");
				List<String> ammolore = new ArrayList<String>();
				ammolore.add(ChatColor.GREEN + "Archer kit arrow");
				weaponmeta.setLore(lore);
				weapon.setItemMeta(weaponmeta);
				ammometa.setLore(ammolore);
				ammo.setItemMeta(ammometa);
				pi.addItem(weapon);
				pi.addItem(ammo);
				pi.setBoots(feet);
				pi.setChestplate(chest);
				pi.setLeggings(legs);
				pi.setHelmet(helmet);
				active = true;
				p.sendMessage(ChatColor.GOLD + "Kit Archer Equiped");
				return true;
			} else if (active == true) {
				pi.remove(Material.BOW);
				pi.remove(Material.ARROW);
				pi.setBoots(null);
				pi.setLeggings(null);
				pi.setHelmet(null);
				pi.setChestplate(null);
				p.sendMessage(ChatColor.GOLD + "Kit Archer Un-Equiped");
				active = false;
				return true;
			}
			return true;
		}
		return true;
	}

}
