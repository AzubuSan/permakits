package me.azubusan.PermaKits;

import java.util.logging.Logger;

import me.azubusan.KitCommands.Archer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class PermaKits extends JavaPlugin implements CommandExecutor, Listener {

	public final Archer arch = new Archer(this);
	public static final Logger logger = Logger.getLogger("Minecraft");
	public static PermaKits plugin;

	@Override
	public void onEnable() {
		getCommand("archer").setExecutor(arch);
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		logger.info(getName() + " Has Been Enabled!");
		getConfig().options().copyDefaults(true);
		saveConfig();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("permakits")) {
			PluginDescriptionFile p = this.getDescription();
			Player player = (Player) sender;
			player.sendMessage(ChatColor.GREEN + "*----* PermaKits Version "
					+ ChatColor.RED + p.getVersion() + ChatColor.BLUE
					+ " by AzubuSan " + ChatColor.GREEN + "*----*");
			player.sendMessage(ChatColor.GOLD
					+ "Source: https://github.com/AzubuSan/PermaKits");
			player.sendMessage(ChatColor.GREEN
					+ "*----*----o0o--------o0o--------o0o----*----*");
			return true;
		}
		return true;
	}

	@Override
	public void onDisable() {
		logger.info(getName() + " Has Been Disabled!");
		saveConfig();
	}
}
